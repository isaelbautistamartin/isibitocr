"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Api = _interopRequireDefault(require("@/services/Api"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  parseImage: function parseImage(formData) {
    return (0, _Api["default"])().post('parse/image', formData);
  }
};
exports["default"] = _default;