import axios from 'axios';

export default () => axios.create({
  baseURL: 'https://api.ocr.space/',
  headers: {
    'Content-Type': 'multipart/form-data',
    'apikey': process.env.VUE_APP_OCR_API_KEY
  },
});
