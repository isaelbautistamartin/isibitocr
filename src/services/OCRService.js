import Api from '@/services/Api';

export default {
  parseImage(formData) {
    return Api().post('parse/image', formData);
  },
};
