# isibit_ocr

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


Clonar el proyecto con el siguiente comando "git clone https://gitlab.com/isaelbautistamartin/isibitocr.git"

Una vez clonado se debe de mover el directorio con el siguiente comando "cd isibitocr"

Dentro del directorio para poser visualizarlo en manera local corra el siguiente comando, "npm run serve", asegurese que se crearon los archivos que se describe mas abajo para que pueda funcionar correctamente.


Se deben de crear dos archivos para las variables de configuración dentro de la carpeta raíz, con los
siguientes nombres ".env.development.local, .env.production.local", ahi se debe de escribir la siguiente 
variable de entorno (VUE_APP_OCR_API_KEY=) pasandole la API_KEY.

En la siguiente linea anexo la api_key ya que esta solo fue creada para realizar pruebas.
VUE_APP_OCR_API_KEY=5bfa0e0ccc88957